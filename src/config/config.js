

// puerto
process.env.PORT = process.env.PORT || 3000

// entorno
process.env.NODE_ENV = process.env.NODE_ENV || 'dev'

// base de datos
let urlDB;

if(process.env.NODE_ENV === 'dev'){
    urlDB = 'mongodb://localhost:27017/lab07DB'
}else{
    urlDB = 'mongodb+srv://chalius:rastachalius@cluster0-0sas4.mongodb.net/test'
}

process.env.URLDB = urlDB